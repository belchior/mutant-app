# Mutant App

## Get Starting

Para rodar a aplicação em modo de desenvolvimento é necessário instalar as dependências:

```shell
npm ci
```

Em seguida rode o comando para iniciar em modo de desenvolvimento

```shell
npm run start:ts
```

### Containers

Esta aplicação está configurada para rodar em `produção` usando container. No Arquivo `Dockerfile` você encontrará instruções para rodar o `build` e colocar o container em execução.

Segue abaixo os comandos para executar a aplicação local:

```shell
docker network create mynetwork
```

```shell
docker run --name elasticsearch -d \
--network mynetwork \
-p 9200:9200 -p 9300:9300 \
-e "discovery.type=single-node" \
elasticsearch:7.7.1
```

```shell
docker build . -t mutant-app
```

```shell
docker run --name mutant-app -d \
-p 8080:8080 \
--network mynetwork \
-e ELASTICSEARCH_URI='http://elasticsearch:9200' \
-e HOST='http://localhost' \
-e NODE_ENV='production' \
-e PORT='8080' \
mutant-app
```

### Docker Compose

A forma **recomentada** para rodar a aplicação é usando o `docker-compose` segue comandos abaixo:

Definindo variáveis de ambiente:

```shell
ELASTICSEARCH_URI='http://elasticsearch:9200'
HOST='http://localhost'
NODE_ENV='production'
PORT=8080
```

Levantando as aplicações `mutant-app` e `elasticsearch`

```shell
docker-compose up
```

Assim que os containers subir e estiverem rodando será possível acessar a aplicação com o comando:

```shell
curl 'http://localhost:8080/users'
```

Os logs armazenados no elasticsearch com o comando:

```shell
curl 'http://localhost:9200/mutant-log/_search'
```

## Funcionalidades

> Faça um programa que carregue a saída dessa URL e mostre os seguintes dados:

> 1\. Os websites de todos os usuários

```shell
curl 'http://localhost:8080/users?fields=website'
```

> 2\. O Nome, email e a empresa em que trabalha (em ordem alfabética).

```shell
curl 'http://localhost:8080/users?fields=name,email,company&sortby=name,asc'
```

> 3\. Mostrar todos os usuários que no endereço contem a palavra ```suite```

```shell
curl 'http://localhost:8080/users?hassuite=true'
```

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

Olá!

Você está participando do processo seletivo para ser um desenvolvedor da Mutant.

Como segunda parte do processo temos um desafio.

O objetivo desse desafio é avaliar o conhecimento dos candidatos. Não existe resolução certa ou errada, avaliaremos com o nível de experiência que for exigido pelas vagas disponíveis no momento. Envie o seu desafio mesmo que você não conclua todas as questões, avaliaremos tudo o que for enviado.

Para participar, você deve se cadastrar no link abaixo para que possa nos enviar o material do seu teste. https://bitbucket.org/

Seguem as instruções:

# Developer challenge

> Sua tarefa é fazer um aplicativo que carregue a saida da URL https://jsonplaceholder.typicode.com/users, que retorna uma lista de usuário em JSON.

> Faça um programa que carregue a saída dessa URL e mostre os seguintes dados:

1. Os websites de todos os usuários

2. O Nome, email e a empresa em que trabalha (em ordem alfabética).

3. Mostrar todos os usuários que no endereço contem a palavra ```suite```

4. Salvar logs de todas interações no elasticsearch

5. EXTRA: Criar test unitário para validar os itens a cima.

Nos utilizamos as linguagens NodeJS e Scala, então vocẽ pode escolher uma destas para criar o programa.

Publique o código no Bitbucket e nos envie o link do mesmo.

> Sua configuração deve ser capaz de ser executado como abaixo:

1. Implantar uma máquina virtual. Para não gerar custos para você e para nós, use o `vagrant` com VirtualBox para provisionar sua instância.

2. Entregar o aplicativo usando o Docker e garantir que ele "sobreviva" nas reinicializações

Tenha em mente que o avaliador irá verificar seu repositório e analisará:

1. Design de código

2. Melhores práticas

O avaliador executará seu código e deverá poder ver que o aplicativo está implementado e em execução.

Coisas que você precisa saber

------------------------

1. Não existe resolução certa ou errada.

2. Este aplicativo inicia na porta 8080 no host local.

3. Nós apreciamos Makefiles :)

4. Lembre-se, na máquina do avaliador, não tem nodejs, ansible ou make.

5. Para ver o conteúdo, acesse: http: // \ <host \> /

Boa sorte!
