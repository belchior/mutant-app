
export const ELASTICSEARCH_URI = process.env.ELASTICSEARCH_URI || 'http://localhost:9200';
export const HOST = process.env.HOST || 'http://localhost';
export const NODE_ENV = process.env.NODE_ENV || 'development';
export const PORT = Number(process.env.PORT) || 8080;
