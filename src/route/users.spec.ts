// @ts-nocheck
import request from 'got';
import { getUsers } from './users';
import { result } from '../utils/mockData';

jest.mock('got');


describe('getUsers', () => {
  beforeEach(() => {
    request.mockReset();
  });

  it('should return a custom error message when the retrieval of users data fails', async () => {
    request.mockImplementationOnce(() => Promise.reject('internal error'));

    const req = {};
    const res = {
      status: jest.fn(() => res),
      json: jest.fn((obj: Object) => obj),
    };

    const result = await getUsers(req, res);

    expect(result.message.search('Error occurred') >= 0).toBe(true);
    expect(request).toBeCalledTimes(1);
    expect(res.status).toBeCalledTimes(1);
    expect(res.json).toBeCalledTimes(1);
  });

  it('should filter users who have "suite" in their address', async () => {
    request.mockImplementationOnce(() => Promise.resolve(result));

    const req = {
      query: {
        hassuite: 'true',
      }
    };
    const res = {
      json: jest.fn((obj: Object) => obj),
    };
    const receivedResult = await getUsers(req, res);
    const expectedResult = [result.body[1]];

    expect(receivedResult).toEqual(expectedResult);
    expect(request).toBeCalledTimes(1);
    expect(res.json).toBeCalledTimes(1);
  });

  it('should only return the fields specified in the "fields" parameter when it was provided', async () => {
    request.mockImplementationOnce(() => Promise.resolve(result));

    const req = {
      query: {
        fields: 'name, email',
      }
    };
    const res = {
      json: jest.fn((obj: Object) => obj),
    };
    const receivedResult = await getUsers(req, res);
    const expectedResult = [
      { name: result.body[0].name, email: result.body[0].email },
      { name: result.body[1].name, email: result.body[1].email },
    ];

    expect(receivedResult).toEqual(expectedResult);
    expect(request).toBeCalledTimes(1);
    expect(res.json).toBeCalledTimes(1);
  });

  it('should return users in alphabetical order when the "sortBy" field was provided', async () => {
    request
      .mockImplementationOnce(() => Promise.resolve(result))
      .mockImplementationOnce(() => Promise.resolve(result));

    let req = {
      query: {
        sortby: 'name,asc',
      }
    };
    const res = {
      json: jest.fn((obj: Object) => obj),
    };
    let receivedResult = await getUsers(req, res);
    let expectedResult = [
      result.body[1],
      result.body[0],
    ];
    expect(receivedResult).toEqual(expectedResult);
    expect(request).toBeCalledTimes(1);
    expect(res.json).toBeCalledTimes(1);

    req = {
      query: {
        sortBy: 'name,desc',
      }
    };
    receivedResult = await getUsers(req, res);
    expectedResult = [
      result.body[0],
      result.body[1],
    ];
    expect(receivedResult).toEqual(expectedResult);
    expect(request).toBeCalledTimes(2);
    expect(res.json).toBeCalledTimes(2);
  });
});
