import express from 'express';
import request from 'got';

import { IUser, TOrder } from '../interface/user';
import { project, sortByKey, filterBySuite } from '../utils/data-transformer';
import { wrap } from '../utils/error-handler';


const router = express.Router();

const fetchUserData = async () => {
  const endpoint = 'https://jsonplaceholder.typicode.com';
  return request(`${endpoint}/users`, { responseType: 'json', timeout: 3000 });
};

export const getUsers = wrap(async (req: express.Request, res: express.Response) => {
  let data;

  try {
    const { body } = await fetchUserData();
    data = body as IUser[];
  } catch (error) {
    return res.status(500).json({ message: 'Error occurred when tried to retrieve user list' });
  }

  if (req.query.hassuite === 'true') {
    data = filterBySuite(data);
  }
  if (typeof req.query.fields === 'string') {
    const keys = req.query.fields.split(/\s*,\s*/g) as Array<keyof IUser>;
    data = project(data, keys);
  }
  if (typeof req.query.sortby === 'string') {
    const [key, order] = req.query.sortby.split(/\s*,\s*/g);
    data = sortByKey(data, key as keyof IUser, order as TOrder);
  }
  return res.json(data);
});

router.get('/', getUsers);
export default router;
