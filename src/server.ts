import express from 'express';

import users from './route/users';
import { ELASTICSEARCH_URI, HOST, PORT } from './environment';
import { handleUnexpectedErrors, logInboundRequest } from './utils/middleware';

const app = express();

const startServer = async () => {
  app.use(logInboundRequest);

  app.use('/users', users);

  app.use(handleUnexpectedErrors);
  app.listen(PORT, () => {
    console.log(`Running a server at ${HOST}:${PORT}`);
    console.log(`Sending logs to elasticsearch at ${ELASTICSEARCH_URI}`);
  });
};

startServer();
