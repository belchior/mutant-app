import { filterBySuite, project, sortByKey } from './data-transformer';
import { data } from '../utils/mockData';


describe('filterBySuite', () => {
  it('should return a list of objects whose address attribute contains the term "suite"', () => {
    const receivedData = filterBySuite(data);
    const expectedData = [ data[1] ];

    expect(receivedData).toEqual(expectedData);
  });
});

describe('project', () => {
  it('should return a list of objects containing only attributes specified in "keys" when the key exists', () => {
    const receivedData = project(data, ['name', 'email']);
    const expectedData = [
      { name: data[0].name, email: data[0].email, },
      { name: data[1].name, email: data[1].email, },
    ];

    expect(receivedData).toEqual(expectedData);
  });
});

describe('sortByKey', () => {
  const list = [
    { 'name': 'Leanne Graham' },
    { 'name': 'Ervin Howell' },
    { 'name': 'Romaguera-Crona' },
    { 'name': 'Deckow-Crist' },
    { 'name': 'Ervin Howell' },
  ];

  it('should return a list of objects sorted by "key" in ascending order', () => {
    const receivedData = sortByKey(list, 'name', 'asc');
    const expectedData = [
      { 'name': 'Deckow-Crist' },
      { 'name': 'Ervin Howell' },
      { 'name': 'Ervin Howell' },
      { 'name': 'Leanne Graham' },
      { 'name': 'Romaguera-Crona' },
    ];

    expect(receivedData).toEqual(expectedData);
  });

  it('should return a list of objects sorted by "key" in descending order', () => {
    const receivedData = sortByKey(list, 'name', 'desc');
    const expectedData = [
      { 'name': 'Romaguera-Crona' },
      { 'name': 'Leanne Graham' },
      { 'name': 'Ervin Howell' },
      { 'name': 'Ervin Howell' },
      { 'name': 'Deckow-Crist' },
    ];

    expect(receivedData).toEqual(expectedData);
  });
});
