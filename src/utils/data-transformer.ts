import R from 'ramda';

import { IUser, TOrder } from '../interface/user';

export const filterBySuite = (data: IUser[]) => {
  return data.filter(user => (
    user.address.suite.search(/suite/i) >= 0
  ));
};

export const project = (data: any[], keys: Array<keyof IUser>) => {
  return data.map(user => R.pick(keys, user));
};

export const sortByKey = (data: any[], key: keyof IUser, order: TOrder) => {
  const dataCopy = data.concat([]);
  dataCopy.sort((prev, curr) => {
    if (prev[key] < curr[key]) return order === 'asc' ? -1 : 1;
    if (prev[key] > curr[key]) return order === 'asc' ? 1 : -1;
    return 0;
  });
  return dataCopy;
};


