import { Client } from '@elastic/elasticsearch';

import { ELASTICSEARCH_URI } from '../environment';

export const elasticsearch = new Client({ node: ELASTICSEARCH_URI });
