// @ts-nocheck
import { logInboundRequest, handleUnexpectedErrors } from './middleware';
import { elasticsearch } from './elasticsearch';


jest.mock('./elasticsearch');


describe('logInboundRequest', () => {
  it('should add to elasticsearch the inbound request', () => {
    elasticsearch.index.mockImplementationOnce(() => ((doc: any) => doc));
    const req = {
      method: 'GET',
      protocol: 'http',
      hostname: 'localhost.com',
      path: '/users',
      query: { hassuite: 'true' },
      originalUrl: '/users?hassuite=true',
    };
    const res = {};
    const next = jest.fn();

    logInboundRequest(req, res, next);

    expect(elasticsearch.index).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledTimes(1);
  });
});

describe('handleUnexpectedErrors', () => {
  beforeEach(() => {
    elasticsearch.index.mockReset();
  });

  it('should only add to elasticsearch the inbound request that throw error', () => {
    elasticsearch.index.mockImplementationOnce(() => ((doc: any) => doc));
    const err = new Error('unexpected error');
    const req = {
      method: 'GET',
      protocol: 'http',
      hostname: 'localhost.com',
      path: '/users',
      query: { hassuite: 'true' },
      originalUrl: '/users?hassuite=true',
    };
    const res = {
      status: jest.fn(() => res),
      json: jest.fn((obj: any) => obj)
    };
    const next = jest.fn(() => {});

    handleUnexpectedErrors(err, req, res, next);

    expect(elasticsearch.index).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledTimes(1);
  });
});
