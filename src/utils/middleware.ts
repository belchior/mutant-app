import express from 'express';

import { elasticsearch } from './elasticsearch';

const getLocation = (req: express.Request) => ({
  method: req.method,
  protocol: req.protocol,
  host: req.hostname,
  path: req.path,
  query: req.query,
  originalUrl: req.originalUrl,
});

export const logInboundRequest = (req: express.Request, res: express.Response, next: Function) => {
  const location = getLocation(req);
  elasticsearch.index({
    index: 'mutant-log',
    body: {
      type: 'request',
      createAt: Date.now(),
      location,
    },
  });
  next();
};

export const handleUnexpectedErrors = (err: Error, req: express.Request, res: express.Response, next: Function) => {
  if (err) {
    const location = getLocation(req);
    elasticsearch.index({
      index: 'mutant-log',
      body: {
        type: 'error',
        createAt: Date.now(),
        location,
        message: err.message
      },
    });
    res.status(500).json({ message: err.message, errorCode: 500 });
  }
  next();
};
